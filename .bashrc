#
# ~/.bashrc
#

# It initializes an interactive shell session and runs whenever bash is started interactively

[[ $- != *i* ]] && return											#If not running interactively, don't do anything

stty -ixon 															#Disable ctrl-s and ctrl-q.
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"	#If readable sources aliases from file
HISTCONTROL=ignoreboth												#Ignore duplicate and spaces bash comands in history
HISTIGNORE="history *:gh *:exit:ls:clear"							#Ignores these comands when saving to HISTFILE
HISTSIZE= HISTFILESIZE=												#Infinite history
export LESSHISTFILE=-												#disable less history
alias wget='wget --hsts-file=${HOME}/.cache/wget-hsts'				#Make history file XDG compliant
alias gpg='gpg --homedir ${HOME}/.local/share/gnupg'				#Make gpg file XDG compliant
export PATH="$PATH:$HOME/.local/bin:$HOME/.config/python/yt:$HOME/.config/python/gallery"	#add user scripts to PATH


[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion	#If readable sources bash completion

case ${TERM} in																						# Change the window title of X terminals
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true																# tells bashrc colours can be used
safe_term=${TERM//[^[:alnum:]]/?}   										# sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"		# If readable source colours from users external file
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"		# If readable source colours from DIR_COLORS
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)								# If match_lhs is empty set it to default colours
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true		# If match_lhs and terminal match use these colours

#ps colours
#firefox https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-bash-ps1-prompt https://misc.flogisoft.com/bash/tip_colors_and_formatting https://youtu.be/tb8s8AJRZQw https://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/ https://www.tecmint.com/customize-bash-colors-terminal-prompt-linux/ https://wiki.archlinux.org/title/Bash/Prompt_customization
if ${use_color} ; then
	# enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval $(dircolors -b ~/.dir_colors)
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval $(dircolors -b /etc/DIR_COLORS)
		fi
	fi
	# gives different colourful prompts depending on if the user is root
	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\e[1;38;5;203m\]\u@\h\[\e[02;01;37m\] \W\[\e[00m\] '
		PS2='\e[1;5;38;5;203m> \e[00m\]'
	fi
	# makes ls, grep, diff, ip, and less colourfull
	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
	alias diff='diff --color=auto'
	alias ip='ip -color=auto'
	# Color man pages
	#https://www.tuxarena.com/2012/04/tutorial-colored-man-pages-how-it-works/
	#https://wiki.archlinux.org/title/Color_output_in_console
	#https://man.archlinux.org/man/less.1#D
	export LESS_TERMCAP_mb=$'\E[1;38;5;203m'		# heading blinking
	export LESS_TERMCAP_md=$'\E[1;38;5;203m'		# heading bold
	export LESS_TERMCAP_me=$'\E[0m'					# normal text   							# end mode
	export LESS_TERMCAP_se=$'\E[0m'					# after manual page standout (bottom left)	# end standout-mode
	export LESS_TERMCAP_so=$'\E[30;48;5;203m'		# after manual page standout (bottom left)	# begin standout-mode
	export LESS_TERMCAP_ue=$'\E[0m'					# after option								# end underline
	export LESS_TERMCAP_us=$'\E[01;33m'				# start option								# begin underline
	#others include
	#export LESS_TERMCAP_mr=$'\E[7m'        # Reverse video					export LESS_TERMCAP_mr=$(tput rev)
	#export LESS_TERMCAP_mh=$'\E[2m'        # Half-bright (dim)				export LESS_TERMCAP_mh=$(tput dim)
	#export LESS_TERMCAP_ZN=$'\E[4m'        # Subscript						export LESS_TERMCAP_ZN=$(tput ssubm)
	#export LESS_TERMCAP_ZV=$'\E[5m'        # Superscript					export LESS_TERMCAP_ZV=$(tput rsubm)
	#export LESS_TERMCAP_ZO=$'\E[6m'        # Sub/superscript off			export LESS_TERMCAP_ZO=$(tput ssupm)
	#export LESS_TERMCAP_ZW=$'\E[24m'       # Clear sub/superscript			export LESS_TERMCAP_ZW=$(tput rsupm)
	export LESS=-R									# allows for --RAW-CONTROL-CHARS
	export GROFF_NO_SGR=1         					# For Konsole and Gnome-terminal
else
	# gives different prompts depending on if the user is root
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

unset use_color safe_term match_lhs sh		# removes variables to keep down pollution
xhost +local:root > /dev/null 2>&1			# enable connections to the X server quietly

### SHOPT
shopt -s checkwinsize						# bash will ajust when terminal size is changed
shopt -s expand_aliases						# aliases are expanded
shopt -s histappend							# enables history appending instead of overwriting.  #139609
shopt -s autocd								# allows cd by typing name of folder
