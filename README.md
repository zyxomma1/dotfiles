# Dotfiles
Dotfiles are the customisation files (their filenames usually begin with a full stop) that are used to personalise your Linux or other Unix-based system. This repository contains my personal dotfiles.

<br/>

## Table of contents <a name="TOC"></a>
- [Installation](#Installation)
- [Silence Boot](#Boot)
- [Disable admin file](#Admin)
- [Show user list on login](#Login)
- [Create a cron job](#Cron)
- [SysRq key](#SysRq)
- [Bashrc](#Bashrc)
- [Inputrc](#Inputrc)
- [Clean-up](#Cleanup)
- [Licence and Status etc.](#End)


<br/><br/>


## [Installation of this repository](#TOC) <a name="Installation"></a>
### Download
```
git clone https://gitlab.com/zyxomma1/dotfiles.git
```
### Move files to your computer
```
cd ~
cd dotfiles 	#cd into downloaded git repo
mv ".bash_logout"	"$HOME/.bash_logout"
mv ".bash_profile"	"$HOME/.bash_profile"
mv ".bashrc"		"$HOME/.bashrc"
mv ".inputrc"		"$HOME/.inputrc"
mv ".profile"		"$HOME/.profile"
```


<br/><br/>


## [Silence Boot](#TOC) <a name="Boot"></a>
[Arch documentation about different ways to silence boot messages](https://wiki.archlinux.org/title/silent_boot)[^7]
This code edits the config file to replace udev with system d. This hides the fsck messages during boot. This is **only for Arch based systems.**
```
sudo sed -i 's/HOOKS="base udev/HOOKS="base systemd/' /etc/mkinitcpio.conf
kernal=$(uname -rv | cut -c 1-4 | tr -d .)
sudo mkinitcpio -p linux$kernal
```
### Disable grub
This removes the grub screen that is displayed before the login screen. Press shift during the boot process to show the grub screen.
```
sudo sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/' /etc/default/grub
sudo update-grub && sudo shutdown --reboot now
```


<br/><br/>


## [Disable admin file](#TOC) <a name="Admin"></a>
This removes the sudo_as_admin_successful file from the home folder
```
echo -e '# Disable ~/.sudo_as_admin_successful file\nDefaults !admin_flag' | sudo tee /etc/sudoers.d/disable_admin_file_in_home
cd ~	#cd into home
rm .sudo_as_admin_successful
```


<br/><br/>


## [Show user list on login](#TOC) <a name="Login"></a>
This changes lightdm so that the users on the system are listed instead of needing to be manually typed.
```
sudo sed -i 's/#greeter-hide-users=false/greeter-hide-users=false/' /etc/lightdm/lightdm.conf
```


<br/><br/>


## [Create a cron job](#TOC) <a name="Cron"></a>
[Cron job creation tutorial by Jay Lacroix](https://youtu.be/v952m13p-b4)[^1]
[Acknowledgement](https://unix.stackexchange.com/questions/48713/how-can-i-remove-duplicates-in-my-bash-history-preserving-order)
The first cron job will remove all duplicates in .bash_history except for their most recent usage. I created this because `HISTCONTROL=erasedups` in .bashrc only erases sequential duplicates. The second starts redshift (a bluelight filter) at startup.
```
export VISUAL=/usr/bin/nano		#Set visual back to nano for editing crontab
crontab -e					#how to edit
```
Paste this command in to crontab
```
#The structure of crontabs
# ┌───────────── minute (0 - 59)
# │ ┌───────────── hour (0 - 23)
# │ │ ┌───────────── day of month (1 - 31)
# │ │ │ ┌───────────── month (1 - 12) or 3 letter abbreviation
# │ │ │ │ ┌───────────── day of week (0 - 6) (Sunday to Saturday;
# │ │ │ │ │                           7 is also Sunday on some systems)
# │ │ │ │ │
# │ │ │ │ │
# * * * * *  command_to_execute


@reboot tac ~/.bash_history | awk '!seen[$0]++' > /tmp/tmpfile  && tac /tmp/tmpfile > ~/.bash_history
@reboot /usr/bin/redshift
```
save with ctrl+o then enter then ctrl+x
```
crontab -l		#list cronjobs to see if the changes have taken affect
```
The following command removes duplicates in bash history using variables. They may not be initialised at boot so I used path names in the cron job.
```
tac "$HISTFILE" | awk '!seen[$0]++' > /tmp/tmpfile  && tac /tmp/tmpfile > "$HISTFILE"
```


<br/><br/>


## [SysRq key](#TOC) <a name="SysRq"></a>
[SysRq documentation](https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html)
[Video walkthrough](https://www.youtube.com/watch?v=ZiX327d8Ys0)
[Written walkthrough](https://forum.manjaro.org/t/howto-reboot-turn-off-your-frozen-computer-reisub-reisuo/3855)
The SysRq (print screen) allows direct communication with the kernel even when the system is unresponsive. This allows the user to avoid a hard reset or power cycle, which could lead to data loss or instability.

### Enable SysRq
SysRq is not enabled on all systems. The following code enables the functions if they are disabled.
```
output=$(cat /proc/sys/kernel/sysrq)
echo "obase=2;$output" | bc #echos out what is currently on in sysrq
enabled=$(($output & 2))						#perform a bitwise AND operation between the current sysrq setting and the value 2

if [ "$enabled" -eq 2 ]; then
    echo "Every SysRq function is enabled"
else
    echo "Every SysRq function is disabled" && echo "Enabling SysRq"
    echo kernel.sysrq=1 | sudo tee --append /etc/sysctl.d/99-sysctl.conf	#enable all SysRq functions permanently
    sudo update-grub
fi
```
Your system needs to be rebooted for sysrq to take effect.
### Soft reboot/power off
To use the SysRq key to reboot input the following key combinations. Replace `b` with `o` to shut your system off instead.
| REISUB | *R*eboot *E*ven *I*f *S*ystem *U*tterly *B*roken |
| - | - |
| alt+syrq+r | Set keyboard to raw format |
| alt+syrq+e | Terminate all processes running |
| alt+syrq+i | Send a SIGKILL to all processes, except for init |
| alt+syrq+s | Attempt to sync all mounted filesystems |
| alt+syrq+u | Remount all mounted filesystems read-only |
| alt+syrq+b | Immediately reboot the system without syncing or unmounting your disks |


<br/><br/>


## [Bashrc](#TOC) <a name="Bashrc"></a>
### Bash prompt
[Colour codes explained by goldilocks on stackexchange](https://unix.stackexchange.com/questions/124407/what-color-codes-can-i-use-in-my-bash-ps1-prompt)[^2]
[Formatting explained by Fabien Loison](https://misc.flogisoft.com/bash/tip_colors_and_formatting)[^3]
[Prompt overview by MentalOutlaw](https://youtu.be/tb8s8AJRZQw)[^4]
[Prompt overview by Vivek Gite](https://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/)[^5]
[Prompt overview by Gabriel Cánepa](https://www.tecmint.com/customize-bash-colors-terminal-prompt-linux/)[^6]
[Arch documentation about different types of prompts](https://wiki.archlinux.org/title/Bash/Prompt_customization)[^7]


<br/><br/>


## [Inputrc](#TOC) <a name="Inputrc"></a>
[Readline documentation](https://wiki.archlinux.org/title/Readline)[^7]
[Keybinding documentation](https://man.archlinux.org/man/readline.3)[^7]
[More Readline documentation](https://web.archive.org/web/20150403162757/http://linux.about.com/library/cmd/blcmdl3_readline.htm)[^8]
[Keybinding to clear screen](https://unix.stackexchange.com/questions/104094/is-there-any-way-to-enable-ctrll-to-clear-screen-when-set-o-vi-is-set)[^9]
[Default keybindings](https://ss64.com/bash/syntax-keyboard.html)[^10]
[Changing cursor in command mode](https://stackoverflow.com/questions/7888387/the-way-to-distinguish-command-mode-and-insert-mode-in-bashs-vi-command-line-ed)[^11]


<br/><br/>


### [Clean-up](#TOC) <a name="Cleanup"></a>
```
cd ~
rm -rf dotfiles						#delete the git folder
```


<br/><br/>


<a name="End"></a>
## Contributing 
No contributions will be accepted because this is how I like to setup my computer. Feel free to fork it if you want.

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.

## Project status
I just add stuff when it needs backed up.

## Authors and acknowledgement
0.  Me (Obviously)
[^1]: [Jay Lacroix](https://jaylacroix.com/)
[^2]: [Stackexchange user goldilocks](https://unix.stackexchange.com/users/25985/goldilocks)
[^3]: [Fabien Loison](https://www.flozz.fr/)
[^4]: [MentalOutlaw](https://www.youtube.com/@MentalOutlaw)
[^5]: [Vivek Gite](https://www.cyberciti.biz/tips/about-us)
[^6]: [Gabriel Cánepa](https://www.tecmint.com/author/gacanepa/)
[^7]: [Arch Linux documentation](https://wiki.archlinux.org)
[^8]: [Juergen Haas](https://www.lifewire.com/juergen-haas-2180099)
[^9]: [Stackexchange user casey](https://wxster.com/)
[^10]: [Simon Sheppard](https://ss64.com/docs/)
[^11]: [Stackoverflow user laktak](https://github.com/laktak)

