#
# ~/.bash_profile
#

# It is used to store environment settings for your terminal and is read whenever a user logs in
[[ -f ~/.bashrc ]] && . ~/.bashrc		#loads bashrc if it exists
