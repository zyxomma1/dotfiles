# Default Apps
export EDITOR=/usr/bin/vi
export VISUAL=/usr/bin/geany
export TERMINAL=/usr/bin/xfce4-terminal
export BROWSER=/usr/bin/brave-browser-stable
export VIDEO="vlc"
export IMAGE="ristretto"
export OPENER="xdg-open"
export PAGER="less"

export QT_QPA_PLATFORMTHEME="qt5ct"
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"


